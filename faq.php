<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>
<body>
	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
		<!--- Banner Section start-->
			<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>FAQ</h2>
						<p><a href="index.html">home</a> &rarr; FAQ</p>
					</div>

			</div>
		</div>
		<!--- Banner Section End-->

		<!-- FAQ Start -->
		<section class="faq">
			<div class="container">
				<div class="row">
					<div class="col-md-6">

						<h2>FAQ Information Section</h2>
						<div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading1">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
											Lorem ipsum dolor sit amet, summo pericula eam ea.
										</a>
									</h4>

								</div>
								<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, summo pericula eam ea. Eu vel erant libris. Ad utroque mediocrem consequuntur eam. Agam nobis assueverit et est. Ex pri animal repudiandae necessitatibus, at cum facete iisque.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading2">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
											Sint diceret in usu. Cu duo exerci regione neglegentur.
										</a>
									</h4>

								</div>
								<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
									<div class="panel-body">
										Sint diceret in usu. Cu duo exerci regione neglegentur, scripta alterum facilis has cu. Admodum probatus periculis nam eu, primis patrioque vim et. Utinam scaevola eum in. Etiam causae intellegebat id has, ei usu doming reprehendunt.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
											Vim at nobis veritus cotidieque, mei ex modo dicta.
										</a>
									</h4>

								</div>
								<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
									<div class="panel-body">
										Vim at nobis veritus cotidieque, mei ex modo dicta. Tollit labores ad pri, his et ubique admodum, at consul officiis cotidieque has. An altera fierent consequat qui, cu elitr legere ridens ius. Vocibus hendrerit usu cu, ad eam case tation ancillae. Ut cum invenire suavitate, error tempor at sit.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading4">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
											Pri wisi civibus at, propriae epicurei id his.
										</a>
									</h4>
								</div>
								<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
									<div class="panel-body">
										Pri wisi civibus at, propriae epicurei id his. Et dico quidam has, et mea illud euripidis temporibus. Esse minimum ius ea. Duo recusabo deseruisse ex, te quaestio hendrerit pri. Unum etiam graeco has at. Wisi vidit eligendi sed at, ad qui vide nulla virtute. Duo ei justo dicam.
									</div>
								</div>
							</div>

						</div>



						<h2>FAQ Information Section</h2>
						<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading5">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
											Lorem ipsum dolor sit amet, summo pericula eam ea.
										</a>
									</h4>

								</div>
								<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, summo pericula eam ea. Eu vel erant libris. Ad utroque mediocrem consequuntur eam. Agam nobis assueverit et est. Ex pri animal repudiandae necessitatibus, at cum facete iisque.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading6">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
											Sint diceret in usu. Cu duo exerci regione neglegentur.
										</a>
									</h4>

								</div>
								<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
									<div class="panel-body">
										Sint diceret in usu. Cu duo exerci regione neglegentur, scripta alterum facilis has cu. Admodum probatus periculis nam eu, primis patrioque vim et. Utinam scaevola eum in. Etiam causae intellegebat id has, ei usu doming reprehendunt.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading7">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
											Vim at nobis veritus cotidieque, mei ex modo dicta.
										</a>
									</h4>

								</div>
								<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
									<div class="panel-body">
										Vim at nobis veritus cotidieque, mei ex modo dicta. Tollit labores ad pri, his et ubique admodum, at consul officiis cotidieque has. An altera fierent consequat qui, cu elitr legere ridens ius. Vocibus hendrerit usu cu, ad eam case tation ancillae. Ut cum invenire suavitate, error tempor at sit.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading8">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
											Pri wisi civibus at, propriae epicurei id his.
										</a>
									</h4>
								</div>
								<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
									<div class="panel-body">
										Pri wisi civibus at, propriae epicurei id his. Et dico quidam has, et mea illud euripidis temporibus. Esse minimum ius ea. Duo recusabo deseruisse ex, te quaestio hendrerit pri. Unum etiam graeco has at. Wisi vidit eligendi sed at, ad qui vide nulla virtute. Duo ei justo dicam.
									</div>
								</div>
							</div>

						</div>

					</div>


					<div class="col-md-6">

						<h2>FAQ Information Section</h2>
						<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading9">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
											Lorem ipsum dolor sit amet, summo pericula eam ea.
										</a>
									</h4>

								</div>
								<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, summo pericula eam ea. Eu vel erant libris. Ad utroque mediocrem consequuntur eam. Agam nobis assueverit et est. Ex pri animal repudiandae necessitatibus, at cum facete iisque.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading10">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
											Sint diceret in usu. Cu duo exerci regione neglegentur.
										</a>
									</h4>

								</div>
								<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
									<div class="panel-body">
										Sint diceret in usu. Cu duo exerci regione neglegentur, scripta alterum facilis has cu. Admodum probatus periculis nam eu, primis patrioque vim et. Utinam scaevola eum in. Etiam causae intellegebat id has, ei usu doming reprehendunt.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading11">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
											Vim at nobis veritus cotidieque, mei ex modo dicta.
										</a>
									</h4>

								</div>
								<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
									<div class="panel-body">
										Vim at nobis veritus cotidieque, mei ex modo dicta. Tollit labores ad pri, his et ubique admodum, at consul officiis cotidieque has. An altera fierent consequat qui, cu elitr legere ridens ius. Vocibus hendrerit usu cu, ad eam case tation ancillae. Ut cum invenire suavitate, error tempor at sit.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading12">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
											Pri wisi civibus at, propriae epicurei id his.
										</a>
									</h4>
								</div>
								<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
									<div class="panel-body">
										Pri wisi civibus at, propriae epicurei id his. Et dico quidam has, et mea illud euripidis temporibus. Esse minimum ius ea. Duo recusabo deseruisse ex, te quaestio hendrerit pri. Unum etiam graeco has at. Wisi vidit eligendi sed at, ad qui vide nulla virtute. Duo ei justo dicam.
									</div>
								</div>
							</div>

						</div>



						<h2>FAQ Information Section</h2>
						<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading13">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
											Lorem ipsum dolor sit amet, summo pericula eam ea.
										</a>
									</h4>

								</div>
								<div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
									<div class="panel-body">
										Lorem ipsum dolor sit amet, summo pericula eam ea. Eu vel erant libris. Ad utroque mediocrem consequuntur eam. Agam nobis assueverit et est. Ex pri animal repudiandae necessitatibus, at cum facete iisque.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading14">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
											Sint diceret in usu. Cu duo exerci regione neglegentur.
										</a>
									</h4>

								</div>
								<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
									<div class="panel-body">
										Sint diceret in usu. Cu duo exerci regione neglegentur, scripta alterum facilis has cu. Admodum probatus periculis nam eu, primis patrioque vim et. Utinam scaevola eum in. Etiam causae intellegebat id has, ei usu doming reprehendunt.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading15">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
											Vim at nobis veritus cotidieque, mei ex modo dicta.
										</a>
									</h4>

								</div>
								<div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
									<div class="panel-body">
										Vim at nobis veritus cotidieque, mei ex modo dicta. Tollit labores ad pri, his et ubique admodum, at consul officiis cotidieque has. An altera fierent consequat qui, cu elitr legere ridens ius. Vocibus hendrerit usu cu, ad eam case tation ancillae. Ut cum invenire suavitate, error tempor at sit.
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading16">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
											Pri wisi civibus at, propriae epicurei id his.
										</a>
									</h4>
								</div>
								<div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
									<div class="panel-body">
										Pri wisi civibus at, propriae epicurei id his. Et dico quidam has, et mea illud euripidis temporibus. Esse minimum ius ea. Duo recusabo deseruisse ex, te quaestio hendrerit pri. Unum etiam graeco has at. Wisi vidit eligendi sed at, ad qui vide nulla virtute. Duo ei justo dicam.
									</div>
								</div>
							</div>

						</div>

					</div>


				</div>
			</div>
		</section>
		<!-- FAQ End -->
		<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>
