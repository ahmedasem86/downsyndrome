<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>
<body>

	<!--Header Start-->
	<?php include('header.php');
		include('admin/get_center_by_id.php');
	?>
	<!--Header End-->
	
		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2> <?= $center_data['name'];?> Details</h2>
						<p><a href="index.html">home</a> &rarr; Center</p>
					</div>
					
			</div>
		</div>
	
	
	<div class="container advice-side">
		<div class="row inner inner-single">
			<div class=" top">
					<div class="bg col-md-6">			
						<div class="img ">
							<img class="center_image" src="/admin/images/profile_images/<?= $center_data['profile_pic'];?>" alt="">
						</div>											
						</div>
					</div>
				<div class="text col-md-6">
				<div class="icon content-center">
							<table class="table table-dark">
								<tbody>
									<tr>
										<td><i class="fa fa-address-book"></i></td>
										<td><?= $center_data['contact_person'];?></td>

									</tr>
								
									<tr>
										<td><i class="fa fa-phone"></i ></td>
										<td><?= $center_data['phone'];?></td>

									</tr>
									
								</tbody>
							</table>
					<h2><?= $center_data['name'];?></h2>
					<p>
					<?= $center_data['description'];?>
					</p>
				
				</div>	
			</div>
		
		</div>
	</div>

	<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>