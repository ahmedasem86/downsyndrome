<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>
<body>

	<!--Header Start-->
	<?php include('header.php');
		include('admin/get_centers.php');
		$types = ['center' , 'school'];
		$governorates = ['Kuwait City' , 'Hawally' , 'AlJahra' , 'AlAhmdi' , 'Mubarak Alkaber' , 'AlFarwaniya'];
	?>
	<!--Header End-->

		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12">
						<h2>Centers</h2>
						<p><a href="index.php">home</a> &rarr; Centers</p>
					</div>
			</div>
	</div>


		<!-- Blog Section Start-->
		<div class="advice blo-page">
		<div class="container">
			<form action="" class="col-md-12">
					<div class="form-group col-md-3">
					<select class="form-select form-control" name="center_type" aria-label="Default select example">
						<option value="all" <?php if(isset($_GET['center_type']) && $_GET['center_type'] == 'all' ) echo 'selected';?>>Center Type</option>
						<option value="0" <?php if( isset($_GET['center_type']) && $_GET['center_type'] == '0' ) echo 'selected';?>>Center</option>
						<option value="1" <?php if( isset($_GET['center_type']) && $_GET['center_type'] == '1' ) echo 'selected';?>>School</option>
					</select>
					</div>
					<div class="form-group col-md-3">
					<select class="form-select form-control" name="governorate" aria-label="Default select example">
					<option value="all"<?php if( !isset($_GET['governorate']) || $_GET['governorate'] == 'all' ) echo 'selected';?>>Any Governorate</option>
		
					<?php foreach($governorates as $i=>$governorate): ?>
						<option value="<?=$i?>" <?php if( isset($_GET['governorate']) && $_GET['governorate'] == "$i" ) echo 'selected';?>><?= $governorate?></option>
							<?php endforeach; ?>
					</select>
					</div>
					<div class="form-group col-md-4">
						<input type="search" placeholder="Search by center name" name="key_word" id="form1" value="<?= (isset($_GET['key_word']))? $_GET['key_word'] : '';?>" class="form-control " />
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary search_btn">
							Search by center name <i class="fas fa-search"></i>
						</button>
					</div>
			</form>
		
			<div class="row">
				<?php foreach($centers as $center): ?>
				<div class="col-sm-6 col-md-4">
				    <div class="thumbnail" style="height: 500px;">
				      	<img src="/admin/images/profile_images/<?= $center['profile_pic'];?>" alt="">
				      	<div class="caption">
					        <h4><a href="advice-details.php?center_id=<?=$center['id'];?>"><?= $center['name'] ?></a></h4>
					        <div class="comment">
					        	<ul>
									<li>
					        			<i class="fa fa-info"></i ><span><?= $types[$center['type']] ?></span>
					        		</li>
									<li>
					        			<i class="fa fa-user"></i ><span><?= $center['contact_person'] ?></span>
					        		</li>
					        		<li>
					        			<i class="fa fa-phone"></i ><span><?= $center['phone'] ?></span>
					        		</li>


					        	</ul>
					        </div>
						    <p class="all" style="height: 70px;"><?= implode(' ', array_slice(explode(' ', $center['description']), 0, 10));?></p>
						    <p class=" col-md-12 submit"><a href="center-details.php?center_id=<?=$center['id']?>" class="btn btn-primary">Read More</a></p>
				      	</div>
				   	</div>
				</div>
				<?php endforeach; ?>
				


			</div>
		</div>
	</div>
	<!-- Blog Section End-->

	<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>
