<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php include('header.php'); ?>
                <?php include('get_users.php'); ?>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                            <?php include('errors.php'); 
                            
                                $user_types = ['administrator','user' , 'doctor'];
                            ?>

                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                    <div class="card-block table-border-style row">
                                    <form action="" class="col-md-10">
                                        <div class="row" style="margin-bottom:10px;">
                                        <select class="form-select form-control col-md-6" name="user_Type" aria-label="Default select example">
                                                            <option value="3" selected>Choose user type</option>
                                                            <option value="0">Administrators</option>
                                                            <option value="1">Users</option>
                                                            <option value="2">Doctors</option>

                                                        </select>
                                        
                                            <button type="submit" class="btn btn-primary admin_button " style="margin-left:10px;">Filter</button>
                                        </div>
                                        </form>
                                        <div class="col-md-2">
                                        <a href="add_user.php" class="btn btn-primary admin_button">Add User</a>
                                        </div>
                                               
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Full name</th>
                                                                <th>Email</th>
                                                                <th>User Type</th>
                                                                <th>phone</th>
                                                                <th>Address</th>
                                                                <th>Description</th>
                                                                <th>Edit</th>
                                                                <th>Delete</th>
                                                                <th>Block</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php foreach($users as $user): ?>
                                                                <tr>
                                                                <td><img width="50" height="50" style="border-radius:30px;"src="/admin/images/profile_images/<?= $user['profile_pic'];?>" alt=""></td>
                                                                <td><?= $user['name'];?></td>
                                                                <td><?= $user['email'];?></td>
                                                                <td><?= $user_types[$user['type']];?></td>
                                                                <td><?= $user['phone'];?></td>
                                                                <td><?= $user['address'];?></td>
                                                                <td><?= $user['description'];?></td>
                                                                <td> <a  class="btn btn-primary admin_button " href="edit_user.php?user_id=<?= $user['id'];?>">
                                                                    <i class="fa fa-edit"></i>
                                                                </a> </td>
                                                                <td>
                                                                <form action="view_users.php" enctype="multipart/form-data" method="post">
                                                                  <input type="hidden" name="user_id" value="<?=$user['id'];?>">
                                                                    <button type="submit" name="delete_user" class="btn btn-primary admin_button "><i class="fa fa-trash"></i></button>
                                                                </form>    
                                                                </td>
                                                                <?php if($user['is_blocked'] == 1):?>
                                                                <td>
                                                                <form action="view_users.php" enctype="multipart/form-data" method="post">
                                                                  <input type="hidden" name="user_id" value="<?=$user['id'];?>">
                                                                    <button type="submit" name="unblock_user" class="btn btn-primary admin_button "><i class="fa fa-user"></i></button>
                                                                </form>      
                                                                </td>
                                                                    <?php else: ?>
                                                                <td>
                                                                <form action="view_users.php" enctype="multipart/form-data" method="post">
                                                                  <input type="hidden" name="user_id" value="<?=$user['id'];?>">
                                                                    <button type="submit" name="block_user" class="btn btn-primary admin_button "><i class="fa fa-user-times"></i></button>
                                                                </form>    
                                                                 </td>
                                                                        <?php endif;?>
                                                            </tr>
                                                                <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>                                  
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
