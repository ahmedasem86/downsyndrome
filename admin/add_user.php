<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php include('header.php'); ?>
                <?php include('get_users.php'); ?>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                            <?php include('errors.php'); 
                            
                                $user_types = ['no','administrator' , 'student'];
                                $doctors_specialities = ['General', 'Developmental pediatricians','Geneticists','Pediatric nurse practitioners' , 'Genetic counselors'];
                            ?>

                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                    <div class="card-block table-border-style">
                                                <?php include('errors.php'); ?>
                                               <h2> Add Users</h2>
                                                <form action="view_users.php" method="post" enctype="multipart/form-data" class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Full name</label>
                                                            <input type="text" class="form-control" name="name" placeholder="Enter Full name" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Email</label>
                                                            <input type="text" class="form-control" name="email" placeholder="Enter email" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Phone</label>
                                                            <input type="text" class="form-control" name="phone" placeholder="Enter Phone" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Profile picture</label>
                                                            <input type="file" class="form-control" name="profile_pic" placeholder="Enter profile pic" >
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Password</label>
                                                            <input type="password" class="form-control" name="password" placeholder="Enter password" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Password confirmation</label>
                                                            <input type="password" class="form-control" name="password_confirmation" placeholder="Enter password" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">User Type</label>
                                                            <select class="form-select form-control" name="user_type" aria-label="Default select example">
                                                                <option selected>select user type</option>
                                                                <option value="0">admin</option>
                                                                <option value="1">user</option>
                                                                <option value="2">doctor</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                        <textarea class="form-control" name="description" placeholder="Doctors description" ></textarea>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Doctor Address</label>
                                                            <input type="text" class="form-control" name="address" placeholder="Enter Doctor Address">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Doctor speciality</label>
                                                            <select class="form-select form-control" name="speciality" aria-label="Default select example">
                                                                <option selected>select doctor speciality</option>
                                                                <?php foreach($doctors_specialities as $i => $speciality): ?>
                                                                <option value="<?=$i?>"><?=$speciality?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    
                                                        <button type="submit" name="add_user" class="btn btn-primary btn_submit">Add User</button>
                                                    </form>

                                            </div>                                
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
