<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php include('header.php'); ?>
                <?php include('get_contacts.php'); ?>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                            <?php include('errors.php'); ?>

                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                    <div class="card-block table-border-style row">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Full name</th>
                                                                <th>Email</th>
                                                                <th>phone</th>
                                                                <th>Mesage</th>
                                                                <th>Delete</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php foreach($messages as $message): ?>
                                                                <tr>
                                                                <td><?= $message['id'];?></td>
                                                                <td><?= $message['name'];?></td>
                                                                <td><?= $message['email'];?></td>
                                                                <td><?= $message['phone'];?></td>
                                                                <td><?= $message['message'];?></td>
                                                                <td>
                                                                <form action="view_contacts.php" enctype="multipart/form-data" method="post">
                                                                  <input type="hidden" name="message_id" value="<?=$message['id'];?>">
                                                                    <button type="submit" name="delete_message" class="btn btn-primary admin_button "><i class="fa fa-trash"></i></button>
                                                                </form>    
                                                                </td>
                                                            </tr>
                                                                <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>                                  
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
