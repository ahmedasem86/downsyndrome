<nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <!-- <img class="img-80 img-radius" src="assets/images/avatar-4.jpg" alt="User-Profile-Image"> -->
                                    <!-- <img class=" img-fluid" src="assets/images/logo.png" style="    width: 129px; background: white;border-radius: 15px;"alt="Theme-Logo" /> -->
                                 
                                        <img class="img-fluid" src="assets/images/logo.png" style="width: 129px; background: white;border-radius: 15px;" alt="Profile-pic" />
                                </div>
                              
                            </div>
                            <!-- <div class="pcoded-navigation-label">Users</div> -->
                            <ul class="pcoded-item pcoded-left-item">
                            <li class="<?= ($_SERVER['REQUEST_URI'] == "/admin/view_users.php")? 'active' : '';?>">
                                            <a href="view_users.php" class="waves-effect waves-dark">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Users</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                            <li class="<?= ($_SERVER['REQUEST_URI'] == "/admin/view_centers.php")? 'active' : '';?>">
                                <a href="view_centers.php" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Centers</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="<?= ($_SERVER['REQUEST_URI'] == "/admin/view_contacts.php")? 'active' : '';?>">
                                <a href="view_contacts.php" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">Contact Us</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            <li class="<?= ($_SERVER['REQUEST_URI'] == "/admin/view_about.php")? 'active' : '';?>">
                                <a href="view_about.php" class="waves-effect waves-dark">
                                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                    <span class="pcoded-mtext">About Us</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            </ul>
                        </div>
                    </nav>