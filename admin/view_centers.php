<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php 
                include('header.php');  
                include('get_users.php'); 
                $governorates = ['Kuwait City' , 'Hawally' , 'AlJahra' , 'AlAhmdi' , 'Mubarak Alkaber' , 'AlFarwaniya'];
                $types = ['center' , 'school'];
                ?>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                            <?php include('errors.php'); 
                                    include('get_centers.php'); 

                            ?>

                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                    <div class="card-block table-border-style row">
                                    <form action="" class="col-md-10">
                                        <div class="row" style="margin-bottom:10px;">
                                        <select class="form-select form-control col-md-6" name="center_type" aria-label="Default select example">
                                                            <option value="3" selected>Choose center type</option>
                                                            <option value="0" <?php if( $_GET['center_type'] == 0 ) echo 'selected';?>>Centers</option>
                                                            <option value="1"  <?php if( $_GET['center_type'] == 1 ) echo 'selected';?>>Schools</option>
                                                        </select>
                                        
                                            <button type="submit" class="btn btn-primary admin_button " style="margin-left:10px;">Filter</button>
                                        </div>
                                        </form>
                                        <div class="col-md-2">
                                        <a href="add_center.php" class="btn btn-primary admin_button">Add Center</a>
                                        </div>
                                               
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Center name</th>
                                                                <th>Center type</th>
                                                                <th>Description</th>
                                                                <th>Governorate</th>
                                                                <th>Address</th>
                                                                <th>Contact Phone</th>
                                                                <th>Contact Person</th>
                                                                <th>Edit</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php foreach($centers as $center): ?>
                                                                <tr>
                                                                <td><img width="50" height="50" style="border-radius:30px;"src="/admin/images/profile_images/<?= $center['profile_pic'];?>" alt=""></td>
                                                                <td><?= $center['name'];?></td>
                                                                <td><?= $types[$center['type']];?></td>
                                                                <td><?= $center['description'];?></td>
                                                                <td><?= $governorates[$center['governorate']];?></td>
                                                                <td><?= $center['address'];?></td>
                                                                <td><?= $center['phone'];?></td>
                                                                <td><?= $center['contact_person'];?></td>
                                                                <td> <a  class="btn btn-primary admin_button " href="edit_center.php?center_id=<?= $center['id'];?>">
                                                                    <i class="fa fa-edit"></i>
                                                                </a> </td>
                                                                <td>
                                                                <form action="view_centers.php" enctype="multipart/form-data" method="post">
                                                                  <input type="hidden" name="center_id" value="<?=$center['id'];?>">
                                                                    <button type="submit" name="delete_center" class="btn btn-primary admin_button "><i class="fa fa-trash"></i></button>
                                                                </form>    
                                                                </td>
                                                            </tr>
                                                                <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>                                  
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
