<!DOCTYPE html>
<html lang="en">
<?php include('head.php');
		$governorates = ['Kuwait City' , 'Hawally' , 'AlJahra' , 'AlAhmdi' , 'Mubarak Alkaber' , 'AlFarwaniya'];

?>
<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php include('header.php'); ?>
                <?php include('get_users.php'); ?>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                            <?php include('errors.php');?>

                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                    <div class="card-block table-border-style">
                                                <?php include('errors.php'); ?>
                                               <h2> Add Center</h2>
                                                <form action="view_centers.php" method="post" enctype="multipart/form-data" class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Center name</label>
                                                            <input type="text" class="form-control" name="name" placeholder="Enter Full name" required>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Center Description</label>
                                                        <textarea class="form-control" name="description" placeholder="Center description" ></textarea>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Choose Governorate</label>
                                                            <select class="form-select form-control" name="governorate" aria-label="Default select example">
                                                               <?php foreach($governorates as $i=> $governorate): ?>
                                                                <option value="<?= $i?>"><?= $governorate?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Profile picture</label>
                                                            <input type="file" class="form-control" name="profile_pic" placeholder="Enter profile pic" >
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Center Address</label>
                                                            <input type="text" class="form-control" name="address" placeholder="Enter center Address">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Contact person</label>
                                                            <input type="text" class="form-control" name="contact_person" placeholder="Enter Doctor Address">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Contact phone</label>
                                                            <input type="text" class="form-control" name="phone" placeholder="Enter center phone">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputEmail1">Center Type</label>
                                                            <select class="form-select form-control" name="type" aria-label="Default select example">
                                                                <option value="0">Center</option>
                                                                <option value="1">School</option>
                                                            </select>
                                                        </div>
                                                        <button type="submit" name="add_center" class="btn btn-primary btn_submit">Add Center</button>
                                                    </form>

                                            </div>                                
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
