<?php
   

    // error_reporting(E_ERROR | E_PARSE);
    if(!defined ('SITE_ROOT')){
        define ('SITE_ROOT', realpath(dirname(__FILE__)));
    }
 //errors array
 $errors = array();
 $success = array();

 $success_message = '';
 //connect to db 
 include('db_connection.php');
 if(isset($_POST['add_user'])){

     $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
     $email = (!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) :'';
     $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
     $user_type = (!empty($_POST['user_type']))? (int)mysqli_real_escape_string($db , (int)$_POST['user_type']) :1;
     $is_doctor = (($_POST['user_type']) == 2 )? 1 : 0;
     $speciality = (!empty($_POST['speciality']))? mysqli_real_escape_string($db , $_POST['speciality']) :'';

     $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
     $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
     $password = (!empty($_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';
     $password_confirmation = (!empty($_POST['password_confirmation']))? mysqli_real_escape_string($db , $_POST['password_confirmation']) : '';
     

     //form validation
     if(empty($name)){ array_push($errors , " user full name is required");}
     if(empty($email)){ array_push($errors , "email is required");}
     if(empty($phone)){ array_push($errors , " user phone is required");}
     if(empty($user_type)){ array_push($errors , " user type is required");}
     if(empty($address) && $user_type == 2){ array_push($errors , " user address is required");}
     if(empty($description) && $user_type == 2){ array_push($errors , " Doctor description is required");}

     if($password != $password_confirmation){ array_push($errors , "Password should match password confirmation");}

     //check db for checking existing username

     $user_check_query = "SELECT * FROM users WHERE  email = '$email' LIMIT 1";
     $result = mysqli_query($db , $user_check_query);
     $user = mysqli_fetch_assoc($result);

     if($user){
         if($user['email'] === $email){ array_push($errors , 'Email already exists'); }
     }
     $profile_pic = '';
     if(!empty($_FILES["profile_pic"]['name'])){
 
         $uploaddir = '/images/profile_images/';
         $uploadfile = $uploaddir . basename($_FILES['profile_pic']['name']);
 
         $file_upload = move_uploaded_file($_FILES['profile_pic']['tmp_name'], SITE_ROOT.$uploadfile);
         if ($file_upload) {
             $profile_pic = basename($_FILES['profile_pic']['name']);
         } else {
           array_push($errors , 'image upload failed'); 
         }
     }
     // register the user if no errors exist
     if(count($errors) == 0){
         $success_message = " user added successfully";

         $password = md5($password);
         $query = "INSERT INTO users (name , email , password , type , phone , address , is_doctor , description ,speciality ,profile_pic ) VALUES ('$name' , '$email' , '$password' , '$user_type' , '$phone' , '$address' , '$is_doctor' , '$description'  ,'$speciality', '$profile_pic')";
         $result_of_the_query = mysqli_query($db , $query);
         if($result_of_the_query == false){
             echo mysqli_error($db);
             die();
         }
     }
 }
 //delete user
 if(isset($_POST['delete_user'])){
    $user_id = $_POST['user_id'];
    $delete_user_query = "DELETE FROM users WHERE id='$user_id'";
    if ($db->query($delete_user_query) === TRUE) {
        $success_message = " user deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
  }
// update user
  if(isset($_POST['update_user'])){

    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
    $email = (!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) :'';
    $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
    $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
    $user_id = (!empty($_POST['user_id']))? mysqli_real_escape_string($db , $_POST['user_id']) :'';

    $user_type = (!empty($_POST['user_type']))? (int)mysqli_real_escape_string($db , (int)$_POST['user_type']) :1;
    $is_doctor = (($_POST['user_type']) == 2 )? 1 : 0;
    $password = (!empty($_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';
    $password_confirmation = (!empty($_POST['password_confirmation']))? mysqli_real_escape_string($db , $_POST['password_confirmation']) : '';
    $profile_pic='';
      //form validation
   
     //form validation
     if(empty($name)){ array_push($errors , " user full name is required");}
     if(empty($email)){ array_push($errors , "email is required");}
     if(empty($phone)){ array_push($errors , " user phone is required");}
     if(empty($address) && $user_type == 2){ array_push($errors , " user address is required");}
     if(empty($description) && $user_type == 2){ array_push($errors , " Doctor description is required");}
     if(empty($user_type)){ array_push($errors , " user type is required");}
     if(!empty($password) && $password != $password_confirmation){ array_push($errors , "Password should match password confirmation");}
 
    //check db for checking existing username
    $user_check_query = "SELECT * FROM users WHERE (email = '$email' ) AND id != '$user_id' LIMIT 1";
   
    $result = mysqli_query($db , $user_check_query);
    $user = mysqli_fetch_assoc($result);
    if($user){
        if($user['email'] === $email){ array_push($errors , 'Email already exists'); }
    }
     if(!empty($_FILES["profile_pic"]['name'])){
 
         $uploaddir = '/images/profile_images/';
         $uploadfile = $uploaddir . basename($_FILES['profile_pic']['name']);
 
         $file_upload = move_uploaded_file($_FILES['profile_pic']['tmp_name'], SITE_ROOT.$uploadfile);
         if ($file_upload) {
             $profile_pic = basename($_FILES['profile_pic']['name']);
         } else {
           array_push($errors , 'image upload failed'); 
         }
     }
    // register the user if no errors exist
    if(count($errors) == 0){
        if(!empty($password) ){
            $password = md5($password);
            $query = "UPDATE users SET  password = '$password' Where id = '$user_id'";
            $result_of_the_query_1 = mysqli_query($db , $query);
            if($result_of_the_query_1 == false){
                echo mysqli_error($db);
                die();
            }
        }

        if(!empty($profile_pic)):
        $query = "UPDATE users SET email = '$email' , phone = '$phone' , address = '$address' ,  name = '$name' , type = '$user_type' , is_doctor='$is_doctor' , profile_pic = '$profile_pic' , description = '$description' Where id = $user_id";
        else:
            $query = "UPDATE users SET email = '$email' , phone = '$phone' , address = '$address' ,  name = '$name' , type = '$user_type' , is_doctor='$is_doctor' ,  description = '$description'  Where id = $user_id";
        endif;
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        if(!empty($profile_pic)):      
              $_SESSION['profile_pic'] =  $profile_pic;
        endif;

        array_push($success , "user updated successfully");

        if(isset($_POST['myprofile'])):
        echo "<script type='text/javascript'>document.location.href='/my_profile.php';</script>";
        else:
        echo "<script type='text/javascript'>document.location.href='view_users.php';</script>";
        endif;
        }
}

if(isset($_POST['unblock_user'])){
    $user_id = $_POST['user_id'];
    $unblock_query = "UPDATE users SET  is_blocked = 0 Where id = '$user_id'";
    if ($db->query($unblock_query) === TRUE) {
        $success_message = " user unblocked successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}

if(isset($_POST['block_user'])){
    $user_id = $_POST['user_id'];
    $unblock_query = "UPDATE users SET  is_blocked = 1 Where id = '$user_id'";
    if ($db->query($unblock_query) === TRUE) {
        $success_message = " user blocked successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
// add center
if(isset($_POST['add_center'])){			
    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
    $governorate = (!empty($_POST['governorate']))?  (int) $_POST['governorate'] :0;
    $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
    $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
    $contact_person = (!empty($_POST['contact_person']))? mysqli_real_escape_string($db , $_POST['contact_person']) :'';
    $type = (!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']) :0;
    $profile_pic = (!empty($_POST['profile_pic']))? (int) $_POST['profile_pic']  :'';
    //check db for checking existing center

    $center_check_query = "SELECT * FROM centers WHERE name = '$name' LIMIT 1";
    $result = mysqli_query($db , $center_check_query);
    $center = mysqli_fetch_assoc($result);

    if($center){
        array_push($errors , 'center name already exists');
    }
    if(!empty($_FILES["profile_pic"]['name'])){

        $uploaddir = '/images/profile_images/';
        $uploadfile = $uploaddir . basename($_FILES['profile_pic']['name']);
        $file_upload = move_uploaded_file($_FILES['profile_pic']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $profile_pic = basename($_FILES['profile_pic']['name']);
        } else {
          array_push($errors , 'image upload failed'); 
        }
    }
    // register the user if no errors exist
    if(count($errors) == 0){
        $success_message = " center added successfully";

        $query = "INSERT INTO centers (name	, description , governorate , address ,	phone,	contact_person,	type,	profile_pic	 ) VALUES ('$name',	'$description',	'$governorate',	'$address',	'$phone' ,	'$contact_person',	'$type'	,'$profile_pic'	)";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
    }
}
// update center
if(isset($_POST['update_center'])){
    $center_id = (!empty($_POST['center_id']))? mysqli_real_escape_string($db , $_POST['center_id']) :'';

    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
    $governorate = (!empty($_POST['governorate']))?  (int) $_POST['governorate'] :0;
    $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
    $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
    $contact_person = (!empty($_POST['contact_person']))? mysqli_real_escape_string($db , $_POST['contact_person']) :'';
    $type = (!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']) :0;
    $profile_pic = (!empty($_POST['profile_pic']))? (int) $_POST['profile_pic']  :'';
  
    //check db for checking existing username
    $center_check_query = "SELECT * FROM centers WHERE (name = '$name' ) AND id != '$center_id' LIMIT 1";
    $results = mysqli_query($db , $center_check_query);
    $center_check_exs = mysqli_fetch_assoc($results);
    if($center_check_exs){
         array_push($errors , 'center name already exists');
    }
     if(!empty($_FILES["profile_pic"]['name'])){
 
         $uploaddir = '/images/profile_images/';
         $uploadfile = $uploaddir . basename($_FILES['profile_pic']['name']);
 
         $file_upload = move_uploaded_file($_FILES['profile_pic']['tmp_name'], SITE_ROOT.$uploadfile);
         if ($file_upload) {
             $profile_pic = basename($_FILES['profile_pic']['name']);
         } else {
           array_push($errors , 'image upload failed'); 
         }
     }
    // register the user if no errors exist
    if(count($errors) == 0){        
        if(!empty($profile_pic)):
            $query = "UPDATE centers SET  phone = '$phone' , address = '$address' ,  name = '$name' , type = '$type' ,contact_person = '$contact_person' , governorate='$governorate' , profile_pic = '$profile_pic' , description = '$description' Where id = $center_id";
        else:
            $query = "UPDATE centers SET  phone = '$phone' , address = '$address' ,  name = '$name' , type = '$type' ,contact_person = '$contact_person' , governorate='$governorate' ,description = '$description' Where id = $center_id";
        endif;
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        $center = $results ->fetch_array(MYSQLI_ASSOC);
        array_push($success , "center updated successfully");
        echo "<script type='text/javascript'>document.location.href='view_centers.php';</script>";

        }
}
 //delete center
 if(isset($_POST['delete_center'])){

    $center_id = $_POST['center_id'];
    $delete_center_query = "DELETE FROM centers WHERE id='$center_id'";
    if ($db->query($delete_center_query) === TRUE) {
        $success_message = " center deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
	
// add advise
if(isset($_POST['add_advice'])){		
    $title = (!empty($_POST['title']))? mysqli_real_escape_string($db , $_POST['title']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
    $user_id = (!empty($_POST['user_id']))? mysqli_real_escape_string($db , $_POST['user_id']) :'';
    $image = '';
    if(empty($title)){ array_push($errors , " advise title is required");}
    if(empty($description)){ array_push($errors , " advise description is required");}
    if(empty($user_id)){ array_push($errors , " author is required");}

    if(!empty($_FILES["image"]['name'])){

        $uploaddir = '/images/profile_images/';
        $uploadfile = $uploaddir . basename($_FILES['image']['name']);
        $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $image = basename($_FILES['image']['name']);
        } else {
          array_push($errors , 'image upload failed'); 
        }
    }
    // register the user if no errors exist
    if(count($errors) == 0){
        $success_message = " advise added successfully";

        $query = "INSERT INTO advice (title	, description  , date ,	user_id,	image) VALUES ('$title',	'$description',	now(),	'$user_id',	'$image')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
    }
}

// update advise
if(isset($_POST['update_advise'])){
    $title = (!empty($_POST['title']))? mysqli_real_escape_string($db , $_POST['title']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
    $advise_id = (!empty($_POST['advise_id']))? mysqli_real_escape_string($db , $_POST['advise_id']) :'';
    $image = '';
    if(empty($title)){ array_push($errors , " advise title is required");}
    if(empty($description)){ array_push($errors , " advise description is required");}
  
   
     if(!empty($_FILES["image"]['name'])){
 
         $uploaddir = '/images/profile_images/';
         $uploadfile = $uploaddir . basename($_FILES['image']['name']);
 
         $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
         if ($file_upload) {
             $image = basename($_FILES['image']['name']);
         } else {
           array_push($errors , 'image upload failed'); 
         }
     }
    // register the user if no errors exist
    if(count($errors) == 0){        
        if(!empty($image)):
            $query = "UPDATE advice SET  title = '$title' , description = '$description', image = '$image' Where id = $advise_id";
        else:
            $query = "UPDATE advice SET  title = '$title' , description = '$description'  Where id = $advise_id";
        endif;
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        array_push($success , "advice updated successfully");
        echo "<script type='text/javascript'>document.location.href='advice.php';</script>";

        }
}

 //delete advice
 if(isset($_POST['delete_advice'])){

    $advice_id = $_POST['advice_id'];
    $delete_advice_query = "DELETE FROM advice WHERE id='$advice_id'";
    if ($db->query($delete_advice_query) === TRUE) {
        $success_message = " advice deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}

// add advise
if(isset($_POST['add_consultation'])){		
    $title = (!empty($_POST['title']))? mysqli_real_escape_string($db , $_POST['title']) :'';
    $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';
      $created_by = (!empty($_POST['created_by']))? mysqli_real_escape_string($db , $_POST['created_by']) :'';
   
    if(empty($title)){ array_push($errors , " advise title is required");}
    if(empty($description)){ array_push($errors , " advise description is required");}
    if(empty($created_by)){ array_push($errors , " author is required");}

   
    // register the user if no errors exist
    if(count($errors) == 0){
        $success_message = " consultation added successfully";

        $query = "INSERT INTO consultations (title	, description  ,created_by) VALUES ('$title',	'$description',	'$created_by')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
    }
}
 //delete advice
 if(isset($_POST['delete_consultation'])){

    $consultation_id = $_POST['consultation_id'];
    $delete_consultation_id_query = "DELETE FROM consultations WHERE id='$consultation_id'";
    if ($db->query($delete_consultation_id_query) === TRUE) {
        $success_message = " consultation deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}


// add comment
if(isset($_POST['add_comment'])){	
    $comment = (!empty($_POST['comment']))? mysqli_real_escape_string($db , $_POST['comment']) :'';
    $consultation_id = (!empty($_POST['consultation_id']))? mysqli_real_escape_string($db , $_POST['consultation_id']) :'';
    $user_id = (!empty($_POST['user_id']))? mysqli_real_escape_string($db , $_POST['user_id']) :'';
    $created_at = (new \DateTime())->format('Y-m-d H:i:s');
    if(empty($comment)){ array_push($errors , " comment  is required");}


   
    // register the user if no errors exist
    if(count($errors) == 0){
        $success_message = " comment added successfully";

        $query = "INSERT INTO comments (comment	, consultation_id  ,user_id , created_At) VALUES ('$comment',	'$consultation_id',	'$user_id' , '$created_at')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
    }
}

//delete advcommentice
if(isset($_POST['delete_comment'])){

    $comment_id = $_POST['comment_id'];
    $delete_comment_query = "DELETE FROM comments WHERE id='$comment_id'";
    if ($db->query($delete_comment_query) === TRUE) {
        $success_message = " comment deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}

// add Contact
if(isset($_POST['add_contact'])){	
    $visitor_name = (!empty($_POST['visitor_name']))? mysqli_real_escape_string($db , $_POST['visitor_name']) :'';
    $visitor_email = (!empty($_POST['visitor_email']))? mysqli_real_escape_string($db , $_POST['visitor_email']) :'';
    $visitor_phone = (!empty($_POST['visitor_phone']))?  $_POST['visitor_phone'] : '';
    $message = (!empty($_POST['visitor_comment']))?  $_POST['visitor_comment'] : '';
    if(empty($visitor_name)){ array_push($errors , " name is required");}
    if(empty($visitor_email)){ array_push($errors , " email is required");}
    if(empty($visitor_phone)){ array_push($errors , "phone is required");}
    if(empty($message)){ array_push($errors , "message is required");}


    // register the user if no errors exist
    if(count($errors) == 0){
        $success_message = " Contact message sent successfully";

        $query = "INSERT INTO contacts (name	, email  , phone ,	message) VALUES ('$visitor_name',	'$visitor_email',	'$visitor_phone',	'$message')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
    }
}

//delete contact
if(isset($_POST['delete_message'])){

    $message_id = $_POST['message_id'];
    $delete_contact_query = "DELETE FROM contacts WHERE id='$message_id'";
    if ($db->query($delete_contact_query) === TRUE) {
        $success_message = " message deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
// update about us
if(isset($_POST['update_about'])){
    $about_us = (!empty($_POST['about_us']))? mysqli_real_escape_string($db , $_POST['about_us']) :'';
   
    if(empty($about_us)){ array_push($errors , " about_us text is required");}
  
  
    // register the user if no errors exist
    if(count($errors) == 0){        

            $query = "UPDATE about_us SET  about_us = '$about_us' Limit 1";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        array_push($success , "Aboutus updated successfully");
        echo "<script type='text/javascript'>document.location.href='/admin/view_about.php';</script>";

        }
}