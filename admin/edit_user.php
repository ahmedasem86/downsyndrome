<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>

<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
                <?php include('header.php'); ?>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                <?php include('sidebar.php'); ?>
                <style>
                    .btn_submit{
                        height: 40px;
                        margin-top: 30px;
                        margin-left: 20px;
                        width: 200px;
                    }
                </style>
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Admin panel</h5>
                                            <p class="m-b-0">Welcome administrator </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.php"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Admin panel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">   
                                            <div class="card-block table-border-style">
                                                <?php include('errors.php');
                                                include('get_user_by_id.php');
                                                ?>
                                               <h2> Edit user</h2>
                                               <form action="view_users.php" method="post" enctype="multipart/form-data" class="row">
                                                     <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Full name</label>
                                                        <input type="text" class="form-control" name="name" value="<?= $user_data['name'] ?>" placeholder="Enter Full name" required>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input type="text" class="form-control" name="email" value="<?= $user_data['email'] ?>" placeholder="Enter email" required>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Phone</label>
                                                        <input type="text" class="form-control" name="phone" value="<?= $user_data['phone'] ?>" placeholder="Enter Phone" required>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Profile picture</label>
                                                        <br>
                                                        <img width="50" height="50" style="border-radius:30px; margin-bottom:5px;"src="/admin/images/profile_images/<?= $user_data['profile_pic'];?>" alt="">
                                                        <input type="file" class="form-control" name="profile_pic"  placeholder="Enter profile pic" >
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Password</label>
                                                        <input type="password" class="form-control" name="password" placeholder="Enter New password if you need to change" >
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Password confirmation</label>
                                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Enter New password if you need to change" >
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">User Type</label>
                                                        <select class="form-select form-control" name="user_type" aria-label="Default select example">
                                                            <option value="0" <?php if( $user_data['type'] == 0 ) echo 'selected';?>>admin</option>
                                                            <option value="1" <?php if( $user_data['type'] == 1 ) echo 'selected';?>>user</option>
                                                            <option value="2" <?php if( $user_data['type'] == 2 ) echo 'selected';?>>doctor</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                     <textarea class="form-control" name="description" value="<?= $user_data['description'] ?>" placeholder="Doctors description" ></textarea>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Doctor Address</label>
                                                        <input type="text" class="form-control" name="address" value="<?= $user_data['address'] ?>" placeholder="Enter Doctor Address">
                                                    </div>
                                                    <input type="hidden" name="user_id" value="<?= $user_data['id'] ?>">
                                                    <button type="submit" name="update_user" class="btn btn-primary btn_submit">update</button>
                                                </form>


                                            </div>                                  
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</body>

</html>
