<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php'); 
include('admin/get_user_by_id.php');
$doctors_specialities = ['General','Developmental pediatricians','Geneticists','Pediatric nurse practitioners' , 'Genetic counselors'];
?>
<body>
	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
		<!--- Banner Section start-->
			<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>Profile Details</h2>
						<p><a href="index.php">home</a> &rarr; Profile Details</p>
					</div>
					
			</div>
		</div>
		<!--- Banner Section start End-->
		<!--Doctor Details Start-->
		<section class="doctor-detail">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="chefs-single">
							<div class="thumb">
								<img src="<?= (isset($user_data['profile_pic']))? '/admin/images/profile_images/'.$user_data['profile_pic'] : '/admin/images/profile_images/download.jpeg';?>"  alt="">
							</div>
							<div class="text">
								<h2><?= $user_data['name']?></h2>
								<?php if($user_data['type'] == 1):?>
								<h3>Parent</h3>
								<?php endif;?>
								<h3><?=(isset($user_data['speciality']) && $user_data['type'] != 1)? $doctors_specialities[$user_data['speciality']]: ''?></h3>
							</div>
							<div class="social">

							</div>
						</div>
					</div>
					<div class="col-md-8">
						
						<!-- Team Detail Tab Start -->
						<div class="doctor-detail-tab">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true">Profile info</a></li>
								<?php (isset($_SESSION['id']) && $_SESSION['id'] == $user_data['id']) ?><li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">Edit profile</a></li>
							</ul>
							
							<!-- Tab Content Start -->
							<div class="tab-content">
								<div class="tab-pane fade active in" id="tab1">
									<div class="row">										
										<div class="col-md-12 content-col">
											<div class="content">
												<h2>Phone</h2>
												<p>
													<?= $user_data['phone'];?>
												</p>
												<h2>Email</h2>
												<p>
													<?= $user_data['email'];?>
												</p>
												<?php if($_SESSION['type'] == 2 ):?>
												<h2>Description</h2>
												<p>
													<?= $user_data['description'];?>
												</p>
												
												<h2>address</h2>
												<p>
												<?= $user_data['address'];?>												</p>
												
												<?php endif;?>

											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab2">
									<div class="row">
										<div class="col-md-12 content-col">
											<div class="content">
												<div class="ask-question">
													<h2>Edit my profile</h2>
													<form action="#" class="form-horizontal" method="post" enctype="multipart/form-data">
														<div class="form-group">
							                                <div class="col-sm-12">
							                                    <input type="text" class="form-control" placeholder="Name" name="name" value="<?= $user_data['name'];?>">
							                                </div>
							                            </div>
														<div class="form-group">
							                                <div class="col-sm-12">
							                                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="<?= $user_data['email'];?>">
							                                </div>
							                            </div>
							                            <div class="form-group">
							                                <div class="col-sm-12">
							                                    <input type="text" class="form-control" placeholder="Phone Number" name="phone" value="<?= $user_data['phone'];?>">
							                                </div>
							                            </div>
							                            <div class="form-group">
							                                <div class="col-sm-12">
							                                    <input type="file" class="form-control" placeholder="Subject" name="profile_pic">
							                                </div>
							                            </div>
														<h3>Change password</h3>
														<div class="form-group">
							                                <div class="col-sm-12">
																<small>Leave this feilds empty unless you need to change password</small>
							                                    <input type="password" class="form-control" placeholder="Passsword" name="password">
																<input type="password" class="form-control" style="margin-top:5px;" placeholder="Password Confirmation" name="password_confirmation">

							                                </div>
							                            </div>
														<?php if($user_data['type'] == 2): ?>
															<h3> Doctor Info</h3>
															<div class="form-group">
							                                <div class="col-sm-12">
							                                    <input type="text" class="form-control" placeholder="address" name="address" value="<?= $user_data['address'];?>">
							                                </div>
							                            </div>
							                            <div class="form-group">
							                                <div class="col-sm-12">
							                                    <textarea name="description" class="form-control" cols="30" rows="10" placeholder="description"><?= $user_data['description'];?></textarea>
							                                </div>
							                            </div>
														<?php endif; ?>
														<input type="hidden" name="myprofile">
														<input type="hidden" name="user_id" value="<?= $user_data['id'];?>">
														<input type="hidden" name="user_type" value="<?= $user_data['type'];?>">
														<div class="form-group">
										                    <div class="col-sm-12">
										                        <input type="submit" name="update_user"value="Update profile" class="btn btn-success">
										                    </div>
										                </div>
													</form>
												</div>									
											</div>
										</div>								
									</div>
								</div>

								
							</div>
							<!-- Tab Content End -->
						</div>
						<!-- Chefs Detail Tab End -->

					</div>
				</div>
			</div>
		</section>
		<!-- Chefs End -->
		<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>