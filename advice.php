<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php'); 
include('admin/get_advice.php');
?>
<body>

	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->

		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12">
						<h2>Advices</h2>
						<p><a href="index.php">home</a> &rarr; Advices</p>
					</div>
			</div>
	</div>


		<!-- Blog Section Start-->
		<div class="advice blo-page">
		<div class="container">
		<?php include('errors.php'); ?>

			<?php if(isset($_SESSION['type']) && $_SESSION['type'] == '2'): ?>
		<div class="col-md-12" >
                 <a href="#" class="btn pull-right" style="color: #fff; background: #7080f5; border-radius: 0;font-size: 14px;display: inline-block;padding: 8px 15px;padding-top:10px; " data-toggle="modal" data-target="#basicModal"> Add Advice <i class="fa fa-plus"></i></a>               
            </div>
			<?php endif; ?>
			<div class="row">
				<?php foreach($advice as $i=>$advise): ?>
				<div class="col-sm-6 col-md-4">
				    <div class="thumbnail" style="height: 510px;">
					<img src="<?= ( $advise['image'])? '/admin/images/profile_images/'. $advise['image']:'img/blog2.jpg' ?> " alt="">
				      	<div class="caption" style="min-height: 305px;">
					        <h4><a href="advice-details.php?id=<?= $advise['id']?>"><?= $advise['title']?></a></h4>
					        <div class="comment">
					        	<ul>
					        		<li>
					        			<i class="fa fa-calendar"></i ><span><?= $advise['date']?></span>
					        		</li>
					        		<li>
					        			<i class="fa fa-user"></i ><span><?= $advise['name']?></span>
					        		</li>
					        	</ul>
					        </div>
							<p class="all" style="height: 90px;"><?= implode(' ', array_slice(explode(' ', $advise['description']), 0, 35));?> ...</p>

						    <p class="submit <?= (isset($_SESSION['type']) &&  $_SESSION['id'] == $advise['user_id'])? 'col-md-5': '';?>"><a href="advice-details.php?id=<?= $advise['id']?>" class="btn btn-primary">Read More</a></p>
							<?php if( isset($_SESSION['id']) && $_SESSION['id'] == $advise['user_id'] ): ?>
							<p class="submit col-md-3">
								<a href="edit_advise.php?id=<?= $advise['id']?>" class="btn btn-primary"   >Edit </a></p>
						    <p class="submit">
								<form action="" method="post">
									<input type="hidden" name="advice_id" value="<?= $advise['id']?>">
								<button  type="submit" name="delete_advice" class="btn btn-primary search_btn">delete</button>
								</form>	
								</p>
								<?php endif;?>

						</div>
				   	</div>
				</div>
				<?php endforeach; ?>
			


			</div>
		</div>
	</div>
	<!-- Blog Section End-->


	<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
		<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Add Advise</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                </div>
                <div class="modal-body">    
                    <form action="advice.php" enctype="multipart/form-data" method="post">
                        <div class="form-group">
                            <label for=""> Advise title</label>
                            <input class="form-control" name="title" type="text" placeholder="Advise title" required>
                        </div>
                        <div class="form-group">
                            <label for=""> Advise description</label>
                            <textarea class="form-control" name="description" id="" cols="30" rows="2" placeholder="Advise description" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for=""> Advise image</label>
                            <input class="form-control" name="image" type="file" placeholder="cover photo" >
                        </div>  
						<input type="hidden" name="user_id" value="<?= $_SESSION['id']?>" >                 

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                  <button  name="add_advice" type="submit" class="btn btn-primary search_btn">Add Advise</button>

                </div>
                </form>

              </div>
            </div>
        </div>

</html>
