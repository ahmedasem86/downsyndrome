<?php

if(!empty($errors)): ?>

<div>
<div class="alert alert-info" role="alert">
    <ol>
    <?php foreach($errors as $error): ?>
        <li> <?= ucfirst($error); ?></li>
    <?php endforeach;?>
    </ol>

    </div>
</div>
<?php endif;?>
