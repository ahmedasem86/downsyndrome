<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>

<body>
    <!--Header Start-->
    <?php include('header.php'); ?>
    <div class="container-fluid service-bg" id="uni">
        <div class="row">
            <div id="universal"></div>
            <div class="col-md-12">
                <h2>Register</h2>
                <p><a href="index.html">home</a> &rarr; Register</p>
            </div>
        </div>
    </div>

    <div class="container-fluid contact">
                <div class="container inner">
                    <div class="row">
                       
                        <div class="col-sm-12 col-md-7 col-lg-8 col-lg-offset-2">
                            <div class="msg">
                                <h3>Register Form</h3>
                                <?php include('errors.php');?>

                                <p>Please register with us through the following form:</p>
                                <div ></div>
                                <form action="/register.php" method="post" >
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Full Name" required="required" id="name" name="name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Enter Email Address" required="required" id="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Phone Number" required="required" id="phone" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Your Address" required="required" id="address" name="address">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Enter Password" required="required" id="password" name="password">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Enter Password confirmation" required="required" id="password_confirmation" name="password_confirmation">
                                    </div>
                                    <input type="submit" value="submit" name="new_user" class="btn btn-success" >
                                </form>
                            </div>
                        </div>

                        
                    </div>

        </div>
    </div>
	<?php include('footer.php'); ?>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/owl.animate.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>
