	<!--Footer Section Start -->
	<section class="footer-main">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-6 col-md-3 col-lg-8 footer-col">
					<h3>Important Links</h3>
					<div class="row">
						<div class="col-md-12">
							<ul class="col-md-6">
								<li><a href="index.php">Home</a></li>
								<li><a href="advice.php">Advices</a></li>
								<li><a href="doctors.php">Our Doctors</a></li>
								<li><a href="consultations.php">Consult</a></li>
							</ul>
                            <ul class="col-md-6">
                                <li><a href="about.php">About</a></li>
								<li><a href="contact.php">Contact</a></li>
								<li><a href="register.php">Register</a></li>
								<li ><a href="signin.php">Signin</a></li>
                            </ul>
						</div>
					</div>
				</div>
			
				<div class="col-sm-6 col-md-3 col-lg-3 footer-col">
					<h3>Contact Us</h3>
				
					<div class="contact-item">
						<div class="icon"><i class="fa fa-phone"></i></div>
						<div class="text">00965-55555555</div>
					</div>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-fax"></i></div>
						<div class="text">00965-555555555</div>
					</div>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-envelope"></i></div>
						<div class="text"> info@downsyndromekw.com</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer Main End -->

	<!-- Footer Bottom Start -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6 copyright">
					Copyright &copy; 2021, Downsyndrome All Rights Reserved.
				</div>
				<div class="col-md-6 footer-menu">
				
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Bottom End -->
	<a href="#" class="scrollup">
		<i class="fa fa-angle-up"></i>
	</a>
