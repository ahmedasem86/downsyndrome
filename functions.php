
<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}    
    //intializing variabes
    $name = '';
    $user_name = '';
    $email = '';
    $phone = '';
    
    //errors array
    $errors = array();
    //connect to db 
    include('admin/db_connection.php');
    if(isset($_POST['new_user'])){
       
        $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
        $email = (!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) :'';
        $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
        $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
        $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'Patient';
        $type = (!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']) :1;
        $is_doctor = (!empty($_POST['is_doctor']))? mysqli_real_escape_string($db , $_POST['is_doctor']) :0;
        $password = (!empty($_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';
        $password_confirmation = (!empty($_POST['password_confirmation']))? mysqli_real_escape_string($db , $_POST['password_confirmation']) : '';

        //form validation
        if(empty($name)){ array_push($errors , "Full name is required");}
        if(empty($address)){ array_push($errors , "Address is required");}
        if(empty($description)){ array_push($errors , "Description is required");}
        if(empty($email)){ array_push($errors , "Email is required");}
        if(empty($phone)){ array_push($errors , "User phone is required");}
        if(empty($password)){ array_push($errors , "Password is required");}
        if($password != $password_confirmation){ array_push($errors , "Password should match password confirmation");}

        //check db for checking existing username

        $user_check_query = "SELECT * FROM users WHERE  email = '$email' LIMIT 1";
        $result = mysqli_query($db , $user_check_query);
        $user = mysqli_fetch_assoc($result);

        if($user){
            if($user['email'] === $email){ array_push($errors , 'Email already exists'); }
        }
       
        // register the user if no errors exist
        if(count($errors) == 0){

            $password = md5($password);
            $query = "INSERT INTO users (name , email , password , type , phone , is_doctor , address , description) VALUES ('$name' , '$email' , '$password' , '$type' , '$phone' , '$is_doctor' , '$address' , '$description')";
            $result_of_the_query = mysqli_query($db , $query);
            if($result_of_the_query == false){
                echo mysqli_error($db);
                die();
            }
            $_SESSION['name'] = $name;
            $_SESSION['success'] = 'You are logged in';
            $_SESSION['email'] = $email;
            $_SESSION['phone'] = $phone;
            $_SESSION['type'] = $type;
            $_SESSION['is_doctor'] = $is_doctor;
            $_SESSION['id'] = $db->insert_id;   

            header('location: index.php');
        }
    }

    //Loging in

    if(isset($_POST['login'])){
        $email =( mysqli_real_escape_string($db , $_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) : '';
        $password = (mysqli_real_escape_string($db , $_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';

        if(empty($email)) {
            array_push($errors , 'email is required'); 
        }
        if(empty($password)) {
            array_push($errors , 'password is required'); 
        }
        if(empty($errors)){
            if($email == 'admin@admin.com' && $password == '1234'){
                $_SESSION['name'] ='admin';
                $_SESSION['type'] = 'administrator'; 
                $_SESSION['success'] = "now you are logged in as administrator";
                $_SESSION['id'] = 1;
                header('location: /admin/index.php');
                return;
            }
            $password = md5($password);

            $query = "SELECT * FROM users WHERE email='$email' AND password='$password';";
            $result = mysqli_query($db ,$query);
            $user = mysqli_fetch_assoc($result);
            if(mysqli_num_rows($result)){
                if($user['type'] == 0){
                    $_SESSION['name'] = $user['name'];
                    $_SESSION['type'] = $user['administrator']; 
                    $_SESSION['success'] = "now you are logged in as administrator";
                    $_SESSION['id'] = $user['id'];
                }else{
                    $_SESSION['type'] = $user['type'];
                }
                $_SESSION['name'] = $user['name'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['phone'] = $user['phone'];
                 $_SESSION['success'] = "now you are logged in";
                $_SESSION['id'] = $user['id'];
                $_SESSION['profile_pic'] = $user['profile_pic'];
                header('location: index.php');
            }else{
                array_push($errors , "wrong email or password");
            }
        }
    }