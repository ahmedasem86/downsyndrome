<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>
<body>
    <!--Header Start-->
    <?php include('header.php'); ?>
    <!--Header End-->
    <div class="container-fluid service-bg" id="uni">
        <div class="row">
            <div id="universal"></div>
            <div class="col-md-12">
                <h2>Contact</h2>
                <p><a href="index.html">home</a> &rarr; Contact</p>
            </div>
        </div>
    </div>

    <div class="container-fluid contact">
                <div class="container inner">
                    <div class="row">
                       <?php if(isset($_GET['message'])): ?>
                        <div class="alert alert-success">
                            <h3> Message Sent successfully</h3>
                        </div>
                        <?php endif;?>
                        <div class="col-sm-12 col-md-7 col-lg-8">
                            <div class="msg">
                                <h3>Contact Form</h3>
                                <p>Please contact us through the following form:</p>
                                <div id="message-contact"></div>
                                <form action="/contact.php?message='sent'" method="post" >
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Full Name" required="required"  name="visitor_name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Enter Email Address" required="required"  name="visitor_email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Phone Number" required="required"  name="visitor_phone">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" id="visitor_comment" name="visitor_comment" cols="30" rows="10" placeholder="Enter Message" required="required"></textarea>
                                    </div>
                                    <input type="submit" name="add_contact" value="SEND MESSAGE" class="btn btn-success" id="submit-contact">
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5 col-lg-4">
                            <div class="last">
                                <h3>Contact Address</h3>
                                <p>Please contact us through the following Address:</p>
                                <div class="last-part cmt">
                                    <div class="icon">
                                        <p><i class="fa fa-map-marker"></i></p>
                                    </div>
                                    <div class="text">
                                        <p>Kuwait 11111-2222, kw</p>
                                    </div>
                                </div>
                                <div class="last-part">
                                    <div class="icon">
                                        <p><i class="fa fa-envelope-o"></i></p>
                                    </div>
                                    <div class="text">
                                        <p>firstemail@gmail.com</p>
                                        <p>secondemail@gmail.com</p>
                                    </div>
                                </div>
                                <div class="last-part">
                                    <div class="icon">
                                        <p><i class="fa fa-phone"></i></p>
                                    </div>
                                    <div class="text">
                                        <p>+111 222 3333</p>
                                        <p>+111 222 4444</p>
                                    </div>
                                </div>
                                <div class="last-part">
                                    <div class="icon">
                                        <p><i class="fa fa-fax"></i></p>
                                    </div>
                                    <div class="text">
                                        <p>(0965) 111 222 3333</p>
                                        <p>(0965) 111 222 4444</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

        </div>
    </div>

	<?php include('footer.php'); ?>


    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/owl.animate.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>
