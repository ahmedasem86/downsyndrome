	<!--Header Start-->
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-1 logo">
					<a href="index.php"><img src="img/logo.png" alt=""></a>
				</div>
				<!-- Nav Start -->
				<div class="col-md-11 nav-wrapper">
					<div class="nav">
						<ul class="sf-menu" id="menu">
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/index.php")? 'current-menu-item' : '';?>"><a href="index.php">Home</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/advice.php")? 'current-menu-item' : '';?>"><a href="advice.php">Advices</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/centers.php")? 'current-menu-item' : '';?>"><a href="centers.php">Centers</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/doctors.php")? 'current-menu-item' : '';?>"><a href="doctors.php">Our Doctors</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/consultations.php")? 'current-menu-item' : '';?>"><a href="consultations.php">Consult</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/about.php")? 'current-menu-item' : '';?>"><a href="about.php">About</a></li>
							<li class="<?= ($_SERVER['REQUEST_URI'] == "/contact.php")? 'current-menu-item' : '';?>"><a href="contact.php">Contact</a></li>

							<?php if(!isset($_SESSION['success'])) : ?>
								<li class="<?= ($_SERVER['REQUEST_URI'] == "/register.php")? 'current-menu-item' : '';?>"><a href="register.php">Register</a></li>
                            <li class="<?= ($_SERVER['REQUEST_URI'] == "/signin.php")? 'current-menu-item' : '';?>"><a href="signin.php">Signin</a></li>
							<?php else:?>  
								<li class="<?= ($_SERVER['REQUEST_URI'] == "/my_profile.php")? 'current-menu-item' : '';?>"><a href="my_profile.php">
									<img width="50" style="border-radius:10px;" src="<?= (isset($_SESSION['profile_pic']))? '/admin/images/profile_images/'.$_SESSION['profile_pic'] : '/admin/images/profile_images/download.jpeg';?>" alt="">
								</a></li>                         
								<li alt="Logout"><a href="index.php?logout=true">    Signout <?= $_SESSION['name'];?> <i class="fas fa-sign-out-alt"></i></a></li>
							<?php endif;?>      
						</ul>
					</div>
				</div>
				<!-- Nav End-->
			</div>
		</div>
	</header>