<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php');
include('admin/get_doctors.php');
$doctors_specialities = ['General','Developmental pediatricians','Geneticists','Pediatric nurse practitioners' , 'Genetic counselors'];

?>
<body>
	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
	<div class="container-fluid service-bg" id="uni">
		<div class="row">
			<div id="universal"></div>
			<div class="col-md-12">
				<h2>Our Doctor</h2>
				<p><a href="index.html">home</a> &rarr; Our Doctor</p>
			</div>
		</div>
	</div>
	<!-- Banner End-->
	<div class="container">
		<div class="row " style="margin-top:30px;">
		<form action="" class="col-md-12">
					
					<div class="form-group col-md-3">
					<select class="form-select form-control" name="speciality" aria-label="Default select example">
					<option value="all"<?php if( !isset($_GET['speciality']) || $_GET['speciality'] == 'all' ) echo 'selected';?>>Any Specialitzation</option>
		
					<?php foreach($doctors_specialities as $i=>$speciality): ?>
						<option value="<?=$i?>" <?php if( isset($_GET['speciality']) && $_GET['speciality'] == "$i" ) echo 'selected';?>><?= $doctors_specialities[$i]?></option>
							<?php endforeach; ?>
					</select>
					</div>
					<div class="form-group col-md-4">
						<input type="search" placeholder="Search by center name" name="key_word" id="form1" value="<?= (isset($_GET['key_word']))? $_GET['key_word'] : '';?>" class="form-control " />
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary search_btn">
							Search by Doctor name <i class="fas fa-search"></i>
						</button>
					</div>
		</form>
		</div>
	
	</div>
	<!--Our Doctor Section Start-->
	<section class="doctor size">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- doctors Container Start -->
					<div class="doctor-inner">
						<?php foreach($doctors as $doctor): ?>
						<div class="col-sm-6 col-md-3 item">
							<div class="inner">
								<div class="thumb">
									<img style="min-height: 250px;" src="<?= ( $doctor['profile_pic'])? '/admin/images/profile_images/'. $doctor['profile_pic']:'img/dc4.jpg' ?>" alt="">
									<div class="overlay"></div>
									<div class="social-icons">
									
									</div>
								</div>
								<div class="text">
									<h3><a href="doctors-detail.php?id=<?=$doctor['id']?>"><?= $doctor['name'] ?></a></h3>
									<h4><?=(isset($doctor['speciality']))? $doctors_specialities[$doctor['speciality']]:'General'; ?></h4>
									<p class="button">
										<a href="doctors-detail.php?id=<?=$doctor['id']?>">Read More</a>
									</p>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
				


					</div>

				</div>
			</div>
		</div>
	</section>
	<!--Our Doctor Section Start-->

	<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>
