<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php'); 
include('admin/get_advice_by_id.php');
?>
<body>

	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
	
		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>Edit advise</h2>
						<p><a href="index.php">home</a> &rarr; Advice</p>
					</div>
					
			</div>
		</div>
	
	
	<div class="container advice-side" style="padding:20px;">
		<div >
		<form action="advice.php" enctype="multipart/form-data" method="post">
                        <div class="form-group">
                            <label for=""> Advise title</label>
                            <input class="form-control" name="title" type="text" value="<?=$advise['title']?>" placeholder="Advise title" required>
                        </div>
                        <div class="form-group">
                            <label for=""> Advise description</label>
                            <textarea class="form-control" name="description" id="" cols="30" rows="2" placeholder="Advise description" required><?=$advise['description']?></textarea>
                        </div>
                        <div class="form-group">
                            <label for=""> Advise image</label>
                            <input class="form-control" name="image" type="file" placeholder="cover photo" >
                        </div>  
						<input type="hidden" name="advise_id" value="<?=$advise['id']?>"" >                 

                </div>
                  <button  name="update_advise" type="submit" class="btn btn-primary search_btn">Save</button>
                </form>
		
		</div>
	</div>
	<?php include('footer.php'); ?>

	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>