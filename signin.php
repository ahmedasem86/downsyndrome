<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php');?>
<body>
    <!--Header Start-->
    <?php include('header.php');?>
    <!--Header End-->
    <div class="container-fluid service-bg" id="uni">
        <div class="row">
            <div id="universal"></div>
            <div class="col-md-12">
                <h2>signin</h2>
                <p><a href="index.html">home</a> &rarr; Signin</p>
            </div>
        </div>
    </div>

    <div class="container-fluid contact">
                <div class="container inner">
                    <div class="row">
                       
                        <div class="col-sm-12 col-md-7 col-lg-8 col-lg-offset-2">
                            <div class="msg">
                                <h3>Signin Form</h3>
                                <?php include('errors.php');?>
                                <p>Please use your credentials to siginin</p>
                                <div ></div>
                                <form action="#" method="post">
                              
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Enter Email Address" required="required"  name="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Enter Password" required="required" id="password" name="password">
                                    </div>
                                    <input type="submit" value="signin" class="btn btn-success" name="login">
                                </form>
                            </div>
                        </div>

                        
                    </div>

        </div>
    </div>
	<?php include('footer.php'); ?>


    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/owl.animate.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>
