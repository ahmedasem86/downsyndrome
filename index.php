<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
	include('head.php');
	include('admin/get_doctors.php');
	include('admin/get_statistics.php');
	include('admin/get_advice.php');

	$doctors_specialities = ['General','Developmental pediatricians','Geneticists','Pediatric nurse practitioners' , 'Genetic counselors'];

?>
<body>
<?php include('header.php');?>
	<!-- Slider Start -->
	<section class="main-slider">

		<div class="slide-carousel owl-item">					
			<div class="item" style="background-image:url(img/slide1.jpg);">
				<div class="overlay"></div>
				<div class="text">
					<div class="this-item">
						<h2>We Have Experienced Doctors</h2>
					</div>
					<div class="this-item">
						<h3>You will get well experienced quality doctors here</h3>
					</div>
					<div class="this-item">
						<p><a href="#">See Details</a></p>
					</div>
				</div>
			</div>					
			<div class="item" style="background-image:url(img/slide2.jpg);">
				<div class="overlay"></div>
				<div class="text">
					<div class="this-item">
						<h2>We Provide Best & Quality Service</h2>
					</div>
					<div class="this-item">
						<h3>We do not compromise anything to keep quality service</h3>
					</div>
					<div class="this-item">
						<p><a href="#">See Details</a></p>
					</div>
				</div>
			</div>
			<div class="item" style="background-image:url(img/slide3.jpg);">
				<div class="overlay"></div>
				<div class="text">
					<div class="this-item">
						<h2>We Are 24 Hours Available</h2>
					</div>
					<div class="this-item">
						<h3>When you will get into a problem, we are always here</h3>
					</div>
					<div class="this-item">
						<p><a href="#">See Details</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
		<!-- Slider End -->

		<!-- Our Choice Section Ends-->



	<!---Service Section Start-->
	<div class="service bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h2>SERVICES</h2>
						<div class="border"></div>
						<p class="down">Our Doctors and Staffs Always Provide Great Quality Services</p>
						<div class="gap-30"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-heart"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Mother Care</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-user-md"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Child Care</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-ambulance"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Parent Care</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
					  	<div class="media-left media-middle">
					      	<p><i class="fa fa-stethoscope"></i></p>
					  	</div>
					  	<div class="media-body">
					    	<h4 class="media-heading">Critical Treatments</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">				
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-medkit"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">All Major Tests</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-flask"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Modern Laboratory</h4>
							<p>Quis non odit sordidos, vanos, leves, futtiles? Primum in nostrane potestate est, quid meminerimus.</p>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>
	<!---Service Section Ends-->

	
	<!---Auto Script Section Starts-->
	<div class="container-fluid auto-script">
		<div class="row">
		<div class="overley"></div>
			<div class="col-md-12">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="media">
		  						<div class="media-left">
								   <i class="fa fa-stethoscope"></i>
								 </div>
								 


								 <div class="media-body">
								   <h4 class="media-heading"><span class="counter"><?=$no_of_doctors['COUNT(*)']?></span></h4>
								   <h5>Expert Doctors</h5>
								 </div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="media">
		  						<div class="media-left">
								   <i class="fa fa-briefcase"></i>
								 </div>
								 <div class="media-body">
								   <h4 class="media-heading"><span class="counter"><?=$no_of_total_registerations['COUNT(*)']?></span></h4>
								   <h5>Total Registerations</h5>
								 </div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="media">
		  						<div class="media-left">
								   <i class="fa fa-user"></i>
								 </div>
								 <div class="media-body">
								   <h4 class="media-heading"><span class="counter"><?= $no_of_total_advices['COUNT(*)']; ?></span></h4>
								   <h5>Advices</h5>
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!---Auto Script Section Ends-->

	
	<!---Meet Our Doctor Section start-->
	<section class="doctor bg-gray">
			<div class="container ">
				<div class="row">
					<div class="col-md-12">
						<div class="heading-col">
							<h2>EXPERT DOCTORS</h2>
							<div class="border"></div>
							<p class="down">Meet and contact all our experienced and expert doctors</p>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						
						<!-- doctors Container Start -->
						<!-- doctors Container Start -->
						<div class="doctor-inner">
						<?php foreach($doctors as $doctor): ?>
						<div class="col-sm-6 col-md-3 item">
							<div class="inner">
								<div class="thumb">
									<img style="min-height:250px;" src="<?= ( $doctor['profile_pic'])? '/admin/images/profile_images/'. $doctor['profile_pic']:'img/dc4.jpg' ?>" alt="">
									<div class="overlay"></div>
									<div class="social-icons">
									
									</div>
								</div>
								<div class="text">
									<h3><a href="doctors-detail.php?id=<?=$doctor['id']?>"><?= $doctor['name'] ?></a></h3>
									<h4><?=(isset($doctor['speciality']))? $doctors_specialities[$doctor['speciality']]:'General'; ?></h4>
									<p class="button">
										<a href="doctors-detail.php?id=<?=$doctor['id']?>">Read More</a>
									</p>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
				


					</div>

					</div>
				</div>
			</div>
		</section>
		<!---Meet Our Doctor Section Ends-->






	<!---Blog Section Start-->
	<div class="advice bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h2>LATEST Advices</h2>
						<div class="border"></div>
						<p class="down">See all doctors latest advices from below</p>
					</div>
				</div>
			</div>
			<div class="row">
			<?php foreach($advice as $i=>$advise): ?>
				<div class="col-sm-6 col-md-4">
				    <div class="thumbnail" style="height: 510px;">
					<img src="<?= ( $advise['image'])? '/admin/images/profile_images/'. $advise['image']:'img/blog2.jpg' ?> " alt="">
				      	<div class="caption" style="min-height: 305px;">
					        <h4><a href="advice-details.php?id=<?= $advise['id']?>"><?= $advise['title']?></a></h4>
					        <div class="comment">
					        	<ul>
					        		<li>
					        			<i class="fa fa-calendar"></i ><span><?= $advise['date']?></span>
					        		</li>
					        		<li>
					        			<i class="fa fa-user"></i ><span><?= $advise['name']?></span>
					        		</li>
					        	</ul>
					        </div>
							<p class="all" style="height: 90px;"><?= implode(' ', array_slice(explode(' ', $advise['description']), 0, 35));?> ...</p>

						    <p class="submit <?= (isset($_SESSION['type']) &&  $_SESSION['id'] == $advise['user_id'])? 'col-md-5': '';?>"><a href="advice-details.php?id=<?= $advise['id']?>" class="btn btn-primary">Read More</a></p>
							<?php if( isset($_SESSION['id']) && $_SESSION['id'] == $advise['user_id'] ): ?>
							<p class="submit col-md-3">
								<a href="edit_advise.php?id=<?= $advise['id']?>" class="btn btn-primary"   >Edit </a></p>
						    <p class="submit">
								<form action="" method="post">
									<input type="hidden" name="advice_id" value="<?= $advise['id']?>">
								<button  type="submit" name="delete_advice" class="btn btn-primary search_btn">delete</button>
								</form>	
								</p>
								<?php endif;?>

						</div>
				   	</div>
				</div>
				<?php if($i > 1){break;} 
			endforeach; ?>
			</div>
		</div>
	</div>
	<!---Blog Section End-->

	<?php include('footer.php'); ?>

	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>