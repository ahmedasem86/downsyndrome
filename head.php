<?php 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION['name']);
    header('location: /signin.php');
}
?>
<?php include('functions.php'); ?>
<?php include('admin/functions.php'); ?>

<head>
	<!-- Meta Tags -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<title>Down syndrome</title>
	
	<!--Favicon Icon-->
	<link rel="icon" type="image/png" href="img/logo.png">

	<!--Style Sheet-->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/superfish.css">
	<link rel="stylesheet" href="css/slicknav.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	
	

	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="style.css">
	
</head>