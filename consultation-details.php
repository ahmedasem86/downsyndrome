<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php'); 
include('admin/get_consultation_by_id.php');
include('admin/get_comments.php');

?>

<body>

	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
	
		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>Consultation Detail</h2>
						<p><a href="index.php">home</a> &rarr; Consultations</p>
					</div>
					
			</div>
		</div>
	
	
	<div class="container advice-side">
		<div class="row inner inner-single">
			<div class=" top">
					<div class="bg">						
						<div class="icon">
							<ul>
								<li><i class="fas fa-users"></i><?= $consultation['name']?></li>

							</ul>
						</div>
					</div>
				<div class="text">
					<h2><?= $consultation['title']?></h2>
					<p>
					<?= $consultation['description']?>
					</p>
				</div>	
			</div>
		
		</div>
	</div>
<?php if($_SESSION['type'] == 2):?>
	<div class="container">
		<form action="#" method="post">
			<div class="form-group col-md-8 col-md-offset-1" >
				<textarea name="comment"  cols="100" rows="5" placeholder="Place your comment here"></textarea>
			</div>
			<div class="form-group">
				<input type="hidden" name="user_id" value="<?= $_SESSION['id']?> ">
				<input type="hidden" name="consultation_id" value="<?= $consultation['id']?> ">
				<button type="submit" class="col-md-1 btn search_btn" name="add_comment"> Reply</button>
			</div>
		</form>
		</div> 
		<?php endif; ?>
	
	<section class="container comments">
		<?php foreach($comments as $comment): ?>
    <article class="comment">
      <a class="comment-img" href="doctors-detail.php?id=<?= $comment['user_id']?>">
        <img src="<?= (isset($comment['profile_pic']))? '/admin/images/profile_images/'.$comment['profile_pic'] : '/admin/images/profile_images/download.jpeg';?>" alt="" width="50" height="50">
      </a>
      <div class="comment-body col-md-6">
        <div class="text "  style="height:55px;">
          <p><?= $comment['comment']?>
		  <?php if($_SESSION['id'] == $comment['user_id']):?>
			<form  action="#" method="post">
				<input type="hidden" name="comment_id" value="<?= $comment['id'] ?>">
				<button type="submit" name="delete_comment"class="pull-right " style="margin-top: -20px; padding: 4px;"> <i class="fa f fa-times"></i></button>
			</form>
		<?php endif;?>
	</p>
        </div>
        <p class="attribution">by <a href="doctors-detail.php?id=<?= $comment['user_id']?>"><?= $comment['name']?></a> at <?= $comment['created_at']?></p>
      </div>
    </article>
	<?php endforeach; ?>
  </section>
  <?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>