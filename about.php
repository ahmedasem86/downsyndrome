<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php include('head.php'); ?>
<body>
	<!--Header Start-->
	<?php include('header.php'); 
	include('admin/get_about_us.php');
	?>
	<!--Header End-->

		<!--- Banner Section start-->
			<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>About Us</h2>
						<p><a href="index.html">home</a> &rarr; About Us</p>
					</div>
			</div>
		</div>
		<!--- Banner Section End-->

	
		<!-- About Start -->
		<section class="about">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="item">
							<h2>About Us</h2>
							<?php if($about_us): ?>
								<p> <?= $about_us['about_us'];?> </p>
								<?php else: ?>
							<p>
								Lorem ipsum dolor sit amet, mea nominavi instructior ex, has cu exerci temporibus. Mei no vero sensibus. Vix at dico expetendis vituperata, te senserit suavitate ius. Ut quo utamur feugiat labores, offendit intellegat vituperata nec ut, sed minimum insolens persequeris ad. Vel facilisi qualisque at.
							</p>
							<p>
								Ne mundi civibus scriptorem his, nullam gloriatur delicatissimi in vim. Eu nec paulo molestiae incorrupte, ex est esse brute altera. Ut dicunt iriure has. Sea facete delenit eloquentiam et. 
							</p>
							<p>
								Mei ut errem legimus periculis, eos liber epicurei necessitatibus eu, facilisi postulant vel no. Ad mea commune disputando, cu vel choro exerci. Pri et oratio iisque atomorum, enim detracto mei ne, id eos soleat iudicabit. Ne reque reformidans mei, rebum delicata consequuntur an sit.
							</p>	
							<?php endif; ?>
						</div>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6">
						<div class="item">
							<img src="img/aboutus.jpeg" alt="about image">
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- About End -->
	
		<?php include('footer.php'); ?>


	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>