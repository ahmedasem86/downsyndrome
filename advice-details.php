<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php 
include('head.php'); 
include('admin/get_advice_by_id.php');
?>
<body>

	<!--Header Start-->
	<?php include('header.php'); ?>
	<!--Header End-->
	
		<div class="container-fluid service-bg" id="uni">
			<div class="row">
				<div id="universal"></div>
					<div class="col-md-12 pt">
						<h2>Advice Details</h2>
						<p><a href="index.php">home</a> &rarr; Advice</p>
					</div>
					
			</div>
		</div>
	
	
	<div class="container advice-side">
		<div class="row inner inner-single">
			<div class=" top">
					<div class="bg">			
						<div class="img">
							<img src="img/advice1.jpg" alt="">
						</div>				
						<div class="icon">
							<ul>
								<li><i class="fa fa-user-md"></i><?= $advise['name']?></li>
								<li><i class="fa fa-calendar"></i ><?= $advise['date']?></li>

							</ul>
						</div>
					</div>
				<div class="text">
					<h2><?= $advise['title']?></h2>
					<p>
					<?= $advise['description']?>
					</p>
				</div>	
			</div>
		
		</div>
	</div>

	



	<!--Footer Section Start -->
	<section class="footer-main">
		<div class="container">
			<div class="row">
			
				<div class="col-sm-6 col-md-3 col-lg-6 footer-col">
					<h3>Important Links</h3>
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="index.html">Home</a></li>
								<li><a href="advice.html">Advices</a></li>
								<li><a href="doctors.html">Our Doctors</a></li>
								<li><a href="faq.html">FAQ</a></li>
								<li><a href="about.html">About</a></li>
								<li><a href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
			
				<div class="col-sm-6 col-md-3 col-lg-3 footer-col">
					<h3>Contact Us</h3>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="text">Unum debitis pro id</div>
					</div>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-phone"></i></div>
						<div class="text">123-456-7878</div>
					</div>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-fax"></i></div>
						<div class="text">123-456-7890</div>
					</div>
					<div class="contact-item">
						<div class="icon"><i class="fa fa-envelope-o"></i></div>
						<div class="text">info@yourdomain.com</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer Main End -->

	<!-- Footer Bottom Start -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6 copyright">
					Copyright &copy; 2021, Downsyndrome All Rights Reserved.
				</div>
				<div class="col-md-6 footer-menu">
				
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Bottom End -->
	<a href="#" class="scrollup">
		<i class="fa fa-angle-up"></i>
	</a>

	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl.animate.js"></script>
	<script src="js/jquery.slicknav.js"></script>
	<script src="js/jquery.counterup.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>